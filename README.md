# Project 12-Nexus-Defense

Singleplayer igra bazirana na zanru "tower defense" u kojoj igrac pokusava da odbrani svoju bazu od najezda neprijatelja. 

## Potrebne biblioteke i kako kompajlirati:

* Instalirati sledeće biblioteke:

`sudo apt-get install qtmultimedia5-dev libqt5multimediawidgets5 libqt5multimedia5-plugins libqt5multimedia5`

* Klonirati ovaj projekat sa gita ili skinuti i ekstraktovati .zip repozitorijuma
<br> git primer: <code> git clone https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/12-nexus-defense

* Namestiti potrebne biblioteke na sledeći način:
<br> [boost 1.74.0](https://dl.bintray.com/boostorg/release/1.74.0/source/boost_1_74_0.tar.gz) - ekstraktovati skinutu verziju bez kompajliranja u folder libraries unutuar foldera projekta. Treba da izgleda ovako /libraries/boost_1_74_0/... 

* Uz pomoć Qt Creator naći i otvoriti NexusDefense.pro fajl i zatim kliknuti zeleno run dugme dole levo. Projekat će biti izgrađen a zatim pokrenut.

## Licence za korišćenje tekstura
CC0 - Public domain sa [opengameart](https://www.opengameart.org)

Linkovi do nekih tekstura koje smo koristili:

[Big pack of tile textures](https://opengameart.org/content/big-pack-of-hand-painted-tiling-textures)

[Backgrounds](https://opengameart.org/content/backgrounds-3)

## Developers

- [Viktor Gizdavic, 146/2017](https://gitlab.com/ArthasWasRight)
- [Helena Jovanovic, 136/2017](https://gitlab.com/helenaJovanovic)
- [Lazar Lukic, 52/2017](https://gitlab.com/Baja-KS)
- [Luka Cerovic, 165/2017](https://gitlab.com/CeraHD)
- [Pavle Savic, 169/2017](https://gitlab.com/PavleSavic)
